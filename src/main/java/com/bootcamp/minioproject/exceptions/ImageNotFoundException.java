package com.bootcamp.minioproject.exceptions;

public class ImageNotFoundException extends RuntimeException {
  public ImageNotFoundException(String imageId) {
    super(imageId);
  }
}
