package com.bootcamp.minioproject.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "images")
@Data
@Accessors(chain = true)
public class ImageT {
    @Id
    private UUID id;
    @Column(length = 100)
    private String name;
    private Long size;
    @Column(length = 300)
    private String link;
    private Long userId;
}
