package com.bootcamp.minioproject.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetImagesResponse {
  @Schema(description = "Список изображений", required = true)
  private List<Image> images;
}
