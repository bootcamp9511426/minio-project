package com.bootcamp.minioproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MinioProjectApplication {

  public static void main(String[] args) {
    SpringApplication.run(MinioProjectApplication.class, args);
  }

}
